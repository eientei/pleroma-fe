import { defineAsyncComponent } from 'vue'
import localeService from '../../services/locale/locale.service.js'

const FederationRestrictionsPopover = {
  name: 'FederationRestrictionsPopover',
  props: [
    'restrictions',
    'domain'
  ],
  components: {
    Popover: defineAsyncComponent(() => import('../popover/popover.vue'))
  },
  methods: {
    formatTime (str) {
      const d = new Date(str)
      if (!d) {
        return
      }
      const locale = localeService.internalToBrowserLocale(this.$i18n.locale)
      return d.toLocaleString(locale)
    }
  }
}

export default FederationRestrictionsPopover
