import generateProfileLink from 'src/services/user_profile_link_generator/user_profile_link_generator'
import { library } from '@fortawesome/fontawesome-svg-core'
import neetImage from '../../assets/neet.png'
import neetUnreadImage from '../../assets/neet-unread.png'
import {
  faBullhorn,
  faTimes
} from '@fortawesome/free-solid-svg-icons'

library.add(
  faBullhorn,
  faTimes
)

const shoutPanel = {
  props: ['floating', 'hide-neet'],
  data () {
    return {
      currentMessage: '',
      last: null,
      channel: null,
      collapsed: true
    }
  },
  computed: {
    messages () {
      return this.$store.state.shout.messages
    },
    unread () {
      return this.last !== this.$store.state.shout.last
    },
    neetImage () {
      return this.unread ? neetUnreadImage : neetImage
    }
  },
  methods: {
    submit (message) {
      this.$store.state.shout.channel.push('new_msg', { text: message }, 10000)
      this.currentMessage = ''
    },
    togglePanel () {
      this.collapsed = !this.collapsed
      this.updateLast()
    },
    updateLast () {
      if (this.$store.state.shout.messages) {
        this.last = this.$store.state.shout.messages[this.$store.state.shout.messages.length - 1].id
      }
    },
    userProfileLink (user) {
      return generateProfileLink(user.id, user.username, this.$store.state.instance.restrictedNicknames)
    }
  },
  watch: {
    messages (newVal) {
      const scrollEl = this.$el.querySelector('.shout-window')

      if (!scrollEl) {
        if (this.last == null) {
          this.updateLast()
        }

        return
      }

      if (scrollEl.scrollTop + scrollEl.offsetHeight + 20 > scrollEl.scrollHeight) {
        this.$nextTick(() => {
          if (!scrollEl) return

          scrollEl.scrollTop = scrollEl.scrollHeight - scrollEl.offsetHeight
        })
      }

      this.updateLast()
    }
  }
}

export default shoutPanel
