const FETCH_FEDERATION_RESTRICTIONS_INTERVAL_MS = 1000 * 60 * 60

export const defaultState = {
  restrictions: {},
  fetchFederationRestrictionsTimer: undefined
}

export const mutations = {
  setFederationRestrictions (state, restrictions) {
    state.restrictions = restrictions
  },
  setFetchFederationRestrictionsTimer (state, timer) {
    state.fetchFederationRestrictionsTimer = timer
  }
}

export const getters = {
  findFederationRestrictions: state => id => {
    return state.restrictions[id] || []
  }
}

const federationRestrictions = {
  state: defaultState,
  mutations,
  getters,
  actions: {
    fetchFederationRestrictions (store) {
      const getFederationRestrictions = async () => {
        const all = await store.rootState.api.backendInteractor.fetchFederationRestrictions()
        const restrictions = {}

        all.forEach(e => {
          const list = (restrictions[e.instance] || [])
          restrictions[e.instance] = list
          list.push(e)
        })

        return restrictions
      }

      return getFederationRestrictions()
        .then(restrictions => {
          store.commit('setFederationRestrictions', restrictions)
        })
    },
    startFetchingFederationRestrictions (store) {
      if (store.state.fetchFederationRestrictionsTimer) {
        return
      }

      const interval = setInterval(() => store.dispatch('fetchFederationRestrictions'), FETCH_FEDERATION_RESTRICTIONS_INTERVAL_MS)
      store.commit('setFetchFederationRestrictionsTimer', interval)

      return store.dispatch('fetchFederationRestrictions')
    },
    stopFetchingFederationRestrictions (store) {
      const interval = store.state.fetchFederationRestrictionsTimer
      store.commit('setFetchFederationRestrictionsTimer', undefined)
      clearInterval(interval)
    }
  }
}

export default federationRestrictions
